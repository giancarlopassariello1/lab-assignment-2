// Giancarlo Passariello (#2138854)
package vehicles;
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manName, int numOfGears, double topSpeed) {
        this.manufacturer = manName;
        this.numberGears = numOfGears;
        this.maxSpeed = topSpeed;
    }

    public String toString() {
        return "Manufacturer: "+this.manufacturer+", Number of Gears: "+this.numberGears+", Max Speed: "+this.maxSpeed;
    }
}


