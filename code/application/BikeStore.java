// Giancarlo Passariello (#2138854)
package application;
import vehicles.Bicycle;
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] b = new Bicycle[4];
        b[0] = new Bicycle("Supercycle", 4, 1000);
        b[1] = new Bicycle("Specialized", 7, 70);
        b[2] = new Bicycle("Canyon", 3, 50);
        b[3] = new Bicycle("Bianchi", 7, 100);

        for (int i = 0; i < b.length; i++) {
            System.out.println(b[i].toString());
        }
    }
}


